package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner input = new Scanner(System.in);
        EncryptOrDecrypt encryptOrDecrypt = new EncryptOrDecrypt();

        String userInput = "";
        String userMsg = "";
        int num = 0;

        try {
                System.out.println("Do you wish to encrypt or decrypt a message");
                userInput = input.nextLine();
                userInput.toLowerCase();
                System.out.println("Enter your message: ");
                userMsg = input.nextLine();
                System.out.println("Enter the key number (1-52");
                num = input.nextInt();

        }catch (Exception e){
            System.out.println("Wrong input");
        }

        if (userInput.equals("decrypt")){
            System.out.println("Your translated text is: \n" + encryptOrDecrypt.decryptMsg(userMsg,num));
        }else if (userInput.equals("encrypt")){
            System.out.println("Your translated text is: \n" + encryptOrDecrypt.encryptMsg(userMsg,num));
        }

    }
}
