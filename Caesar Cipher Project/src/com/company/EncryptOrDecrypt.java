package com.company;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class EncryptOrDecrypt {


    private int num = 0;
    private String userMsg = "";
    private String userInput = "";
    private ArrayList<Character> alphabet = new ArrayList<>();
    private ArrayList<Character> alphabetCopy = new ArrayList<>();


    String encryptMsg(String input, int n) {


        for (char alpha = 'A'; alpha <= 'Z'; alpha++) {
            alphabet.add(alpha);

        }

        for (char alpha = 'a'; alpha <= 'z'; alpha++) {
            alphabet.add(alpha);
            alphabetCopy.add(alpha);
        }

        for (char alpha = 'A'; alpha <= 'Z'; alpha++) {

            alphabetCopy.add(alpha);
        }

        for (int i=0; i< n ; i++){
            char last = alphabetCopy.remove(alphabetCopy.size()-1);
            alphabetCopy.add(0,last);

        }

        for (int i=0; i<input.length(); i++) {
            for (int j = 0; j < 52; j++) {
                if (input.charAt(i) == alphabet.get(j)) {
                    userMsg = userMsg + alphabetCopy.get(j);
                }else if(input.charAt(i) == ' ') {
                    userMsg = userMsg + ' ';
                    break;
                }
            }
        }

        try {
            Files.write(Paths.get("text.log"), userMsg.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return userMsg;
    }


    // decrption
    String decryptMsg(String input, int n) {

        for (char alpha = 'A'; alpha <= 'Z'; alpha++) {
            alphabet.add(alpha);
            alphabetCopy.add(alpha);
        }

        for (char alpha = 'a'; alpha <= 'z'; alpha++) {
            alphabet.add(alpha);
            alphabetCopy.add(alpha);
        }

        for (char alpha = 'A'; alpha <= 'Z'; alpha++) {


        }

        for (int i=0; i< n ; i++){
            char last = alphabetCopy.remove(alphabetCopy.size()-1);
            alphabetCopy.add(0,last);

        }

        try {
            Files.write(Paths.get("text1.log"), input.getBytes(StandardCharsets.US_ASCII));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
           userInput = String.valueOf(Files.readAllLines(Paths.get("text1.log")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(userInput);


        for (int i=0; i< input.length(); i++) {
            for (int j = 0; j < 52; j++) {
                if (input.charAt(i) == alphabet.get(j)) {
                    userMsg = userMsg + alphabetCopy.get(j);
                }else if(input.charAt(i) == ' ') {
                    userMsg = userMsg + ' ';
                    break;
                }
            }
        }


        return userMsg;
    }

}

