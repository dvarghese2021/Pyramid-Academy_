package com.example.parts.controller;

import com.example.parts.entity.Parts;
import com.example.parts.service.PartsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PartsController {

    private final PartsService partsService;

@Autowired
    public PartsController(@Qualifier("partsServiceIMPL") PartsService partsService) {
        this.partsService = partsService;
    }
    //http://localhost:8080/retrieveAllParts
    @GetMapping("/retrieveAllParts")
    public List<Parts> findAll() {
    return partsService.findAll();
    }


    //http://localhost:8080/addParts
    @PostMapping("/addParts")
    public Parts addParts(@RequestBody Parts theParts) {
        //also, just in case they pass an id in JSON .... set id to 0
        //this is to force a save of new item .... instead of update
        theParts.setSku(0);

        //This will call the employeeDqoImpl.save method to save a new employee
        //through the employeeService
        partsService.saveOrUpdate(theParts);
        return theParts;
    }

    //This is a PUT request to update an existing employee.
    //http://localhost:8080/updateParts
    @PutMapping("/updateParts")
    public Parts updateParts(@RequestBody Parts updateParts) {
        //Notice theEmployee.setId(0); this will execute an update instead of a create
        partsService.saveOrUpdate(updateParts);
        return updateParts;
    }

    //This is a DELETE request to delete an existing employee.
    //http://localhost:8080/deleteParts/1
    @DeleteMapping("/deleteParts/{partsId}")
    public String deleteParts(@PathVariable int partsId) {
        //This will execute the deleteByID.
        partsService.deleteById(partsId);
        return "Deleted parts id : " + partsId;
    }

    //http://localhost:8081/getParts/1
    @GetMapping("/getParts/{partsId}")
    public String getParts(@PathVariable int partsId) {
        //This will execute the deleteByID.
        partsService.findById(partsId);

        return "parts by id : " + partsId + " " + partsService.findById(partsId);
    }

}
