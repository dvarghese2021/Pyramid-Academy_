package com.example.parts.dao;

import com.example.parts.entity.Parts;

import java.util.List;

public interface PartsDAO {

    List<Parts> findAll();
    Object findById(int partsId);
    void saveOrUpdate(Parts theParts);
    void deleteById(int partsId);


}
