package com.example.parts.entity;


import javax.persistence.*;

@Entity
@Table(name = "parts")
public class Parts {

    @Id //This will map the primary key.
    @GeneratedValue(strategy = GenerationType.IDENTITY) //This will auto increment your primary key
    @Column(name = "sku") //This is mapping the primary key to the sku column in the table.
    private int sku;

    @Column(name = "brand")
    private String brand;

    @Column(name = "category")
    private String category;

    @Column(name = "yearmade")
    private String yearmade;

    @Column(name = "description")
    private String description;

    public Parts() {
    }

    public Parts(int sku, String brand, String category, String yearmade, String description) {

        this.category = category;
        this.yearmade = yearmade;
        this.description = description;
    }

    public int getSku() {
        return sku;
    }

    public void setSku(int sku) {
        this.sku = sku;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getYearmade() {
        return yearmade;
    }

    public void setYearmade(String yearmade) {
        this.yearmade = yearmade;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Register{" +
                "sku=" + sku +
                ", brand='" + brand + '\'' +
                ", category='" + category + '\'' +
                ", yearmade='" + yearmade + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
