package com.example.parts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PartsRegistryApplication {

	public static void main(String[] args) {
		SpringApplication.run(PartsRegistryApplication.class, args);
	}

}
