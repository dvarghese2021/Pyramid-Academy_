package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public  class Ship {
    static int count=0;  // instance counter

    private static String name1 = "";
    private static String name2 = "";
    char[][] pl1 = new char[9][9];
    char[][] pl2 = new char[9][9];
    private String pos;
    private String direction;

    Player1Coordinates play1 = new Player1Coordinates();
    Player2Coordinates play2 = new Player2Coordinates();





    public void initializeBoard1(){
        int num=1;
        int counter =1;
        System.out.println("  0 1 2 3 4 5 6 7 8  ");
        System.out.printf("0 ");
        for (int i = 0; i < pl1.length; i++) {
            for(int j=0; j< pl1.length; j++) {
                player1[i][j] = '~';
               // player2[i][j] = '~';
                System.out.printf(String.valueOf(player1[i][j])+" ");
                if (counter == 9){
                    System.out.println("\n"+ " ");
                    if(num<=8) {
                        System.out.printf(String.valueOf(num) + " ");
                    }
                    counter =0;
                    num++;
                }
                counter++;

            }
        }




    }

    private void initializeBoard2() {

        int num1=1;
        int counter1 =1;
        System.out.println("  0 1 2 3 4 5 6 7 8  ");
        System.out.printf("0 ");
        for (int i = 0; i < pl1.length; i++) {
            for(int j=0; j< pl1.length; j++) {
                player2[i][j] = '~';

                System.out.printf(String.valueOf(player2[i][j])+" ");
                if (counter1 == 9){
                    System.out.println("\n"+ " ");
                    if(num1<=8) {
                        System.out.printf(String.valueOf(num1) + " ");
                    }
                    counter1 =0;
                    num1++;
                }
                counter1++;

            }
        }
    }


    public String getPos() {

        return pos;
    }

    public void setPos(String pos) {

        this.pos = pos;
    }

    public String getDirection() {
        return direction;
    }

    public char[][] setDirection(String direction, char[][] player1) {
        this.direction = direction;
        return player1;
    }

    Play play = new Play();
    char[][] player1 = new char[9][9];
    char[][] player2 = new char[9][9];

    Scanner input = new Scanner(System.in);
    ArrayList<Class<? extends Ship>> placeShip = new ArrayList<>();
    List<Ship> list = new ArrayList<>();



    public Ship() {
        placeShip.add(BattleShip.class);
        placeShip.add(CarrierShip.class);
        placeShip.add(Cruiser.class);
        placeShip.add(Destroyer.class);
        placeShip.add(Submarine.class);
    }

    public Ship(String name1) {
        this.name1 = name1;
        this.name2 = name2;
        if (count == 0){
            initializeBoard1();
        }else{
            initializeBoard2();
        }
        BattleShip a = new BattleShip();
        CarrierShip b = new CarrierShip();
        Cruiser c = new Cruiser();
        Destroyer d = new Destroyer();
        Submarine e = new Submarine();
        list.add(a);
        list.add(b);
        list.add(c);
        list.add(d);
        list.add(e);


        for (int i = 0 ; i< list.size(); i++){

                System.out.println("please enter the coordinates for your " + list.get(i).getClass().getSimpleName() + ":");
            try {
                pos = input.next();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            System.out.println(pos);
                if (pos.length()<2){
                    System.out.println("Enter valid input (1,2)");
                }

            list.get(i).setPos(pos);
            System.out.println("Place horizontally or vertically (h or v)?");
            try {
                direction = input.next();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            if (count > 0){
                System.out.println("count for player 2 "+count);
               pl1 = list.get(i).setDirection(direction, player2);
            }else{
                System.out.println("count for player 1 "+count);
            pl2 = list.get(i).setDirection(direction, player1);
        }}


        count++;

    }


    public static String getName1() {
        return name1;
    }

    public static String getName2() {
        return name2;
    }

    private void set() {

    }

    private void setDirection() {

    }


}
