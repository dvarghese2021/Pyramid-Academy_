package com.company;

import java.util.Scanner;

public class HitOrMiss {

    Scanner input = new Scanner(System.in);
    String playerInput ;
    int x=0;
    int y=0;
    static char[][] p1copy = new char[9][9];
    static char[][] p2copy = new char[9][9];

    // No. 	Class of ship 	Size
    //         c 	Carrier 	5
    //         b 	Battleship 	4
    //         a 	Cruiser 	3
    //        s 	Submarine 	3
    //         d 	Destroyer 	2


    public void checkHitorMiss(char[][] board1, char[][] board2, String player1, String player2){

        boardIni(p1copy);
        boardIni(p2copy);


        boolean gameover = true;
       int hitCounter1 = 0;
       int hitCounter2 = 0;

        while(gameover) {
           int n = 0;
           int m = 0;
           System.out.println(player1 + " enter the coordinates for an attack: ");
            try {
                playerInput = input.next();
            } catch (Exception e) {
                e.printStackTrace();
            }
            x = Integer.parseInt(String.valueOf(playerInput.charAt(0)));
           y = Integer.parseInt(String.valueOf(playerInput.charAt(2)));

                   if (board1[x][y] == '~') {
                      /// board1[x][y] = 'm';
                       System.out.println("You missed!");
                       p1copy[x][y]='m';
                       p1Board(p1copy);

                   } else if (board1[x][y] == 'a' || board1[x][y] == 'b' || board1[x][y] == 'c' || board1[x][y] == 'd' || board1[x][y] == 's') {
                       System.out.println("You hit");

                       p1copy[x][y]='x';
                       p1Board(p1copy);
                       hitCounter1++;

                   }else{
                       System.out.println("Error");
                   }

// player 2
           int n1 = 0;
           int m1 = 0;
           System.out.println(player2 + " enter the coordinates for an attack: ");
            try {
                playerInput = input.next();
            } catch (Exception e) {
                e.printStackTrace();
            }
            x = Integer.parseInt(String.valueOf(playerInput.charAt(0)));
           y = Integer.parseInt(String.valueOf(playerInput.charAt(2)));

                   if (board2[x][y] == '~') {

                       p2copy[x][y]='m';
                       System.out.println("You missed!");
                       p2Board(p2copy);


                   } else if (board2[x][y] == 'a' || board2[x][y] == 'b' || board2[x][y] == 'c' || board2[x][y] == 'd' || board2[x][y] == 's') {
                       System.out.println("You hit");

                       p2copy[x][y]='x';
                       p2Board(p2copy);
                       hitCounter2++;

                   }else{
                       System.out.println("Error");
                   }


           if (hitCounter1 == 17 || hitCounter2 == 17){
               gameover = false;
           }
       }


    }

    public void p1Board(char[][] a){
        int num1=1;
        int counter1 =1;
        System.out.println("  0 1 2 3 4 5 6 7 8  ");
        System.out.printf("0 ");
        for (int i = 0; i <a.length; i++) {
            for(int j=0; j< a.length; j++) {
               // a[i][j] = '~';

                System.out.printf(String.valueOf(a[i][j])+" ");
                if (counter1 == 9){
                    System.out.println("\n"+ " ");
                    if(num1<=8) {
                        System.out.printf(String.valueOf(num1) + " ");
                    }
                    counter1 =0;
                    num1++;
                }
                counter1++;

            }
        }
    }

    public void p2Board(char[][] b){
        int num1=1;
        int counter1 =1;
        System.out.println("  0 1 2 3 4 5 6 7 8  ");
        System.out.printf("0 ");
        for (int i = 0; i < b.length; i++) {
            for(int j=0; j< b.length; j++) {
               // b[i][j] = '~';

                System.out.printf(String.valueOf(b[i][j])+" ");
                if (counter1 == 9){
                    System.out.println("\n"+ " ");
                    if(num1<=8) {
                        System.out.printf(String.valueOf(num1) + " ");
                    }
                    counter1 =0;
                    num1++;
                }
                counter1++;

            }
        }
    }

    public void boardIni(char[][] c){
        int num1=1;
        int counter1 =1;

        for (int i = 0; i < c.length; i++) {
            for(int j=0; j< c.length; j++) {
                c[i][j] = '~';

            }
        }
    }
    public void boardIniMain(char[][] c){
        int num1=1;
        int counter1 =1;
        System.out.println("  0 1 2 3 4 5 6 7 8  ");
        System.out.printf("0 ");
        for (int i = 0; i < c.length; i++) {
            for(int j=0; j< c.length; j++) {
                 c[i][j] = '~';

                System.out.printf(String.valueOf(c[i][j])+" ");
                if (counter1 == 9){
                    System.out.println("\n"+ " ");
                    if(num1<=8) {
                        System.out.printf(String.valueOf(num1) + " ");
                    }
                    counter1 =0;
                    num1++;
                }
                counter1++;

            }
        }
    }
}
