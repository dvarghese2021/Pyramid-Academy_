package com.company;

public class Player2Coordinates {

    // No. 	Class of ship 	Size
    //         c 	Carrier 	5
    //         b 	Battleship 	4
    //         a 	Cruiser 	3
    //        s 	Submarine 	3
    //         d 	Destroyer 	2
    static char[][] p2 = new char[9][9];


// char[][] p2 = new char[9][9];

    public char[][] checkCoordinates(int x, int y, String direction, char battleShipletter, char[][] player2){
        p2=player2;
        int count = 1;
        int num = 1;
        int size =0;

        if (battleShipletter == 'a'){
            size = 3;
        }else if (battleShipletter == 'b'){
            size = 4;
        }else if (battleShipletter == 'c'){
            size = 5;
        }else if (battleShipletter == 'd'){
            size = 2;
        }else if (battleShipletter == 's'){
            size = 3;
        }

        player2[x][y]=battleShipletter;

        System.out.println(direction);
        if (direction.equals("v")){

            for (int i=0;i<size; i++){
                player2[x+i][y]=battleShipletter;
            }

        }else if (direction.equals("h")){
            for (int i=0;i<size; i++){
                player2[x][y+i]=battleShipletter;
            }
        }

        System.out.println("  0 1 2 3 4 5 6 7 8  ");
        System.out.printf("0 ");
        for (int i = 0; i < player2.length; i++) {
            for(int j=0; j< player2.length; j++) {
                //board[i][j] = '~';
                System.out.printf(String.valueOf(player2[i][j])+" ");
                if (count == 9){
                    System.out.println("\n"+ " ");
                    if(num<=8) {
                        System.out.printf(String.valueOf(num) + " ");
                    }
                    count =0;
                    num++;
                }
                count++;

            }
        }


        return player2;
    }

    public static char[][] getP2() {
        return p2;
    }



}
