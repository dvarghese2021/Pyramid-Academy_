package com.company;

import javax.swing.*;
import java.awt.*;
import java.util.Scanner;

public class Arena extends JFrame {
    JPanel a = new JPanel();

    static Human[][] humans = new Human[10][10];
    public Arena() {
        setTitle("The Arena");
        setSize(500, 500);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBackground(Color.blue);
        a.setLayout(new GridLayout(5, 5));
        a.setBackground(Color.green);


        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                humans[i][j] = new Human();

                a.add(humans[i][j]);
            }
        }

        int goblinx = (int) (Math.random() * (4 - 0 + 1) + 1);
        int gobliny = (int) (Math.random() * (4 - 0 + 1) + 1);

        humans[goblinx][gobliny].setText("\uD83D\uDC7A");
        int count = 0;
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {

                if (humans[i][j].getText() == "") {

                    humans[i][j + 1].setText("\u263A");
                    count++;
                    break;
                }

            }
            if (count > 0) {
                break;
            }
        }

        add(a);
        setVisible(true);

        boolean compact1 = false;
        boolean compact2 = false;


        while (check(humans)) {


            if (check(humans)){
                int rad = (int) (Math.random()*(2-1+1)+1);
                System.out.println("Game Over");
                if (rad ==1){
                    System.out.println("Human Wins!");
                }else{
                    System.out.println("Goblin wins");
                }
                break;
            }

        }



    }
    public static boolean check(Human humans[][]){
        System.out.println("Enter d/a/w/s button on keyboard for move human");
        Scanner input = new Scanner(System.in);
        boolean compact1 = false;
        int humPosi = 0;
        int humPosj = 0;
        int golPosi = 0;
        int golPosj = 0;
        String key = null;
        try {
            key = input.next();
        } catch (Exception e) {
            e.printStackTrace();
        }
        int count = 0;
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {

                if (humans[i][j].getText() == "\u263A") {
                    System.out.println(i);
                    System.out.println(j);
                    humPosi = i;
                    humPosj = j;
                }
                if (humans[i][j].getText() == "\uD83D\uDC7A") {
                    System.out.println(i);
                    System.out.println(j);
                    golPosi = i;
                    golPosj = j;
                }

            }

        }

        if (humPosi == (golPosi + 1) || humPosi == (golPosi - 1) || humPosj == (golPosj + 1) || humPosj == (golPosj - 1)) {
            System.out.println("Human and Goblin too close! Compact Started");
            compact1 = true;
        }


        Movement.keyCheck(key,humans);
        return compact1;
    }


}




