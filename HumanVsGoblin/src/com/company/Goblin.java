package com.company;

import javax.swing.*;
import java.awt.*;

public class Goblin extends JButton {

    private int xPos;
    private int yPos;

    @Override
    public void setBackground(Color bg) {
        super.setBackground(Color.green);
    }

    public Goblin (){

    }

    public int getXPos(){
        return xPos;
    }
    public int getYPos(){
        return yPos;
    }
    public void setXPos(){
        this.xPos = 2;
    }
    public void setYPos(){
        this.yPos = 4;
    }

}
