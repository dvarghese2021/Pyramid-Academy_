package com.company;

import java.util.*;

public class Main {


    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        String playAgain = "NO";


        UserInput checkUser = new UserInput();

        HashMap<Integer, String> ticTac = new HashMap<>();




            for (int i = 1; i < 10; i++) {
                ticTac.put(i, "-");

            }


                checkUser.checkUserInput(ticTac);
        System.out.println("Game over");

            System.out.println("Do you want to play again? (yes or no)");
            playAgain = input.next();
           playAgain =  playAgain.toUpperCase();
            while (playAgain.equals("YES")) {
                for (int i = 1; i < 10; i++) {
                    ticTac.put(i, "-");

                }
                checkUser.checkUserInput(ticTac);
            }



    }

}

    class UserInput {

        Scanner input = new Scanner(System.in);
        String user = "";
        String com = "";
        int position = 0;
        int n = 0;
        int count =0;
        Random random = new Random();


        public void checkUserInput(HashMap<Integer, String> ticTac) {

            String user = "";
            int n = 0;
            int count =0;
            System.out.println("Welcome to Tic-Tac-Toe!");
            System.out.println("Do you want to be X or O");
            try {
                user = input.next();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (user.equals("X") || user.equals("x")) {
                com = "O";
            } else {
                com = "X";
            }

            System.out.println("The computer will go first");

            while (n < 4) {
                position = (int) (Math.random() * (9 - 1 + 1) + 1);
                while (checkPositionCom(position, com, ticTac)) {
                    break;
                }
                System.out.println("Com picks its position");
                display(ticTac);
                if  (checkWhoWins(ticTac)){
                    break;
                }
                System.out.println("What is your next move?(1-9)");
                try {
                    position = input.nextInt();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                while (checkPositionUser(position, user, ticTac)) {
                    break;
                }
                display(ticTac);

                n++;


            }


        }

        public boolean checkWhoWins(HashMap<Integer, String> ticTac){
            if (ticTac.get(1).equals(user) && ticTac.get(2).equals(user) && ticTac.get(3).equals(user)){
                System.out.println("User wins");
                display(ticTac);
                return true;
            } else if(ticTac.get(4).equals(user) && ticTac.get(5).equals(user) && ticTac.get(6).equals(user)) {
                System.out.println("User wins");
                display(ticTac);
                return true;
            }else if(ticTac.get(7).equals(user) && ticTac.get(8).equals(user) && ticTac.get(9).equals(user)) {
                System.out.println("User wins");
                display(ticTac);
                return true;
            }else if(ticTac.get(1).equals(user) && ticTac.get(4).equals(user) && ticTac.get(7).equals(user)) {
                System.out.println("User wins");
                display(ticTac);
                return true;
            }else if(ticTac.get(2).equals(user) && ticTac.get(5).equals(user) && ticTac.get(8).equals(user)) {
                System.out.println("User wins");
                display(ticTac);
                return true;
            }else if(ticTac.get(3).equals(user) && ticTac.get(6).equals(user) && ticTac.get(9).equals(user)) {
                System.out.println("User wins");
                display(ticTac);
                return true;
            }else if(ticTac.get(1).equals(user) && ticTac.get(5).equals(user) && ticTac.get(9).equals(user)) {
                System.out.println("User wins");
                display(ticTac);
                return true;
            }else if(ticTac.get(3).equals(user) && ticTac.get(5).equals(user) && ticTac.get(7).equals(user)) {
                System.out.println("User wins");
                display(ticTac);
                return true;
            } else if (ticTac.get(1).equals(com) && ticTac.get(2).equals(com) && ticTac.get(3).equals(com)){
                System.out.println("The computer has beaten you! You lose.");
                display(ticTac);
                return true;
            } else if(ticTac.get(4).equals(com) && ticTac.get(5).equals(com) && ticTac.get(6).equals(com)) {
                System.out.println("The computer has beaten you! You lose.");
                display(ticTac);
                return true;
            }else if(ticTac.get(7).equals(com) && ticTac.get(8).equals(com) && ticTac.get(9).equals(com)) {
                System.out.println("The computer has beaten you! You lose.");
                display(ticTac);
                return true;
            }else if(ticTac.get(1).equals(com) && ticTac.get(4).equals(com) && ticTac.get(7).equals(com)) {
                System.out.println("The computer has beaten you! You lose.");
                display(ticTac);
                return true;
            }else if(ticTac.get(2).equals(com) && ticTac.get(5).equals(com) && ticTac.get(8).equals(com)) {
                System.out.println("The computer has beaten you! You lose.");
                display(ticTac);
                return true;
            }else if(ticTac.get(3).equals(com) && ticTac.get(6).equals(com) && ticTac.get(9).equals(com)) {
                System.out.println("The computer has beaten you! You lose.");
                display(ticTac);
                return true;
            }else if(ticTac.get(1).equals(com) && ticTac.get(5).equals(com) && ticTac.get(9).equals(com)) {
                System.out.println("The computer has beaten you! You lose.");
                display(ticTac);
                return true;
            }else if(ticTac.get(3).equals(com) && ticTac.get(5).equals(com) && ticTac.get(7).equals(com)) {
                System.out.println("The computer has beaten you! You lose.");
                display(ticTac);
                return true;
            }else if (n >3){
                System.out.println("its a tie! ");
                return false;
            }
           // System.out.println("its a tie! ");
            count++;
            return false;
        }

        private boolean checkPositionUser(int position, String str, HashMap<Integer, String> ticTac) {

            Scanner input = new Scanner(System.in);

            if (ticTac.get(position).equals("-")) {
                ticTac.put(position, str);
                return true;
            } else {
                while (!ticTac.get(position).equals("-")) {

                    System.out.println("This position already taken! Try another!");

                    if (str.equals(user)) {
                        position = (int) (Math.random() * (9 - 1 + 1) + 1);
                        while (!ticTac.get(position).equals("-")) {
                            position = (int) (Math.random() * (9 - 1 + 1) + 1);
                        }
                    } else {
                        position = input.nextInt();
                    }
                }

                ticTac.put(position, user);
                return false;
            }


        }

        private boolean checkPositionCom(int position, String com, HashMap<Integer, String> array) {
            Scanner input = new Scanner(System.in);

            if (array.get(position).equals("-")) {
                array.put(position, com);
                return true;
            } else {
                while (!array.get(position).equals("-")) {

                    if (com.equals(com)) {
                        position = (int) (Math.random() * (9 - 1 + 1) + 1);
                        while (!array.get(position).equals("-")) {
                            position = (int) (Math.random() * (9 - 1 + 1) + 1);
                        }
                    } else {
                        try {
                            position = input.nextInt();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                array.put(position, com);
                return false;

            }
        }

        public  void display(HashMap<Integer, String> hasDat) {
            System.out.println(hasDat.get(1) + "|" + hasDat.get(2) + "|" + hasDat.get(3));
            System.out.println("-----");
            System.out.println(hasDat.get(4) + "|" + hasDat.get(5) + "|" + hasDat.get(6));
            System.out.println("-----");
            System.out.println(hasDat.get(7) + "|" + hasDat.get(8) + "|" + hasDat.get(9));
        }
    }

